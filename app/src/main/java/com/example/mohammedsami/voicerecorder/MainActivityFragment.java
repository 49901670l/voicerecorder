package com.example.mohammedsami.voicerecorder;

import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.Manifest;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};

    private MediaRecorder recorder = null;
    private MediaPlayer payer = null;

    private ImageView logo;
    private FloatingActionButton play;
    private FloatingActionButton record;
    private FloatingActionButton stop;

    String fileName = null;
    boolean gravanr = false;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        fileName = getContext().getExternalFilesDir(null).getAbsolutePath() + "/audiorecord.3gp";
        logo = view.findViewById(R.id.logo);
        play = view.findViewById(R.id.play);
        record = view.findViewById(R.id.record);
        stop = view.findViewById(R.id.stop);

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_voice_black_24dp));

                if (recorder != null){
                    recorder.stop();
                    recorder.reset();
                    recorder.release();
                    recorder = null;
                    gravanr = false;
                }
                else if (payer != null){
                    payer.stop();
                    payer.release();
                    payer = null;
                }
            }
        });

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_record_voice_over_black_24dp));

                if (recorder == null && payer == null){
                    payer = new MediaPlayer();
                    try {
                        payer.setDataSource(fileName);
                        payer.prepare();
                        gravanr = true;
                        payer.start();
                        payer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                stop.callOnClick();
                            }
                        });
                    } catch (IOException e) {
                        Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG);
                    }
                }
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recorder == null){
                    logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_skip_next_black_24dp));
                    try {
                        recorder = new MediaRecorder();
                        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        recorder.setOutputFile(fileName);
                        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                        recorder.prepare();
                    } catch (IOException e) {
                        Log.d("MediaRecorder", "No sha pogut configurar");
                    }
                    recorder.start();
                }
                else {
                    stop.callOnClick();
                }
            }
        });
        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted){
            Toast.makeText(getContext(), "Permission needed", Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
    }
}
